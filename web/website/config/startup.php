<?php

$autoloader = \Zend_Loader_Autoloader::getInstance();
$autoloader->registerNamespace('Gather');
Pimcore::getEventManager()
    ->attach('document.preAdd', ['\Gather\Event\PostHandler', 'preAdd']);
