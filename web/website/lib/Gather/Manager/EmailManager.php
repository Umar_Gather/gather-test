<?php

namespace Gather\Manager;

use Pimcore\Model\Document;
use Pimcore\Log\ApplicationLogger;

class EmailManager
{

    /**
     * @var EmailManager $instance
     */
    private static $instance;

    /**
     * @var \Pimcore\View\Helper\Url() $urlHelper
     */
    private $urlHelper;


    private function __construct()
    {
        //external instances not allowed
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
            self::$instance->init();
        }

        return self::$instance;
    }

    private function init()
    {
        //anything needed here
    }

    /**
     * @return string
     */
    public function getUrl(array $urlOptions = [], $name = null, $reset = false, $encode = true)
    {
        if (!$this->urlHelper) {
            $this->urlHelper = new \Pimcore\View\Helper\Url();
        }

        return \Pimcore\Tool::getHostUrl() . $this->urlHelper->url($urlOptions, $name, $reset, $encode);
    }

    public function getCurrentLocale()
    {
        if (\Zend_Registry::isRegistered("Zend_Locale")) {
            $locale = \Zend_Registry::get("Zend_Locale");
        } else {
            $locale = new \Zend_Locale("en");
            \Zend_Registry::set("Zend_Locale", $locale);
        }

        return (string) $locale;
    }

    /**
     * @param $contactForm \Pimcore\Model\Object\ContactForn
     * @throws \Exception
     */
    public function triggerContactUsEmail($contactForm)
    {

        $document = Document\Email::getByPath('/email');

        if (!$document) {
            throw new \Exception('/email template is not set up');
        }

        $params = [
            'name' => $contactForm->getName(),
            'email'    => $contactForm->getEmail(),
            'reason'   => $contactForm->getPhone(),
            'comment'  => $contactForm->getMessage()
        ];

        $this->sendEmail($document->getTo(), $document, $params);
    }

     /**
     * @param $appliantForm \Pimcore\Model\Object\Vaccancy
     * @throws \Exception
     */
    public function triggerNewApplicantEmail($appliantForm)
    {

        $document = Document\Email::getByPath('/application');

        if (!$document) {
            throw new \Exception('/application template is not set up');
        }

        $params = [
            'name' => $appliantForm->getName(),
            'email'    => $appliantForm->getEmail(),
            'phone'   => $appliantForm->getPhone(),
            'porfolio' => $appliantForm->getPortfolio(),
            'github' => $appliantForm->getGithub(),
            'question1' => $appliantForm->getQuestion1(),
            'question2' => $appliantForm->getQuestion2(),
            'question3' => $appliantForm->getQuestion3()
        ];

        $this->sendEmail($document->getTo(), $document, $params);
    }

    /**
     * @param      $to
     * @param      $document Document\Email
     * @param      $params
     * @param null $additionalMailOptions
     */
    public function sendEmail($to, $document, $params, $additionalMailOptions=null)
    {
        try {
            if (!\Zend_Controller_Action_HelperBroker::hasHelper('viewRenderer') ) {
                $removeHelper = true;
                \Zend_Controller_Action_HelperBroker::addHelper(new \Pimcore\Controller\Action\Helper\ViewRenderer());
            } else {
                $removeHelper = false;
            }
            $mail = new \Pimcore\Mail();
            //check that the to address has not already been set in the document.
            if ($document->getTo() != $to) {
                $mail->setTo($to);
            }
            $mail->setDocument($document);
            $mail->setParams($params);

            if (is_array($additionalMailOptions)) {
                foreach($additionalMailOptions as $key=>$value) {
                    $setter = 'set' . ucfirst($key);
                    if (method_exists($mail, $setter)) {
                        $mail->$setter($value);
                    }
                }
            }

            // enable automatic text version of the email
            $mail->enableHtml2textBinary();

            $mail->send();

            if ($removeHelper === true) {
                \Zend_Controller_Action_HelperBroker::removeHelper('viewRenderer');
            }

        } catch(\Exception $e) {
            $log = ApplicationLogger::getInstance("EmailManager", true);
            $log->logException('sendEmail error', $e);
        }

    }


}
