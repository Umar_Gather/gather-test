<?php

namespace Gather\Event;

use Pimcore\Model\Document;

class PostHandler
{
    public static function preAdd(\Zend_EventManager_Event $e)
    {
        $target = $e->getTarget();

        if (
            $target instanceof Document\Page &&
            $target->getController() == 'post' &&
            $target->getAction() == 'post'
        ) {
            $parent = $target->getParent();

            while (
                (!($parent instanceof Document\Page) ||
                $parent->getController() != 'post' ||
                $parent->getAction() != 'post-list') &&
                $parent->getParent()
            ) {
                $parent = $parent->getParent();
            }

            if ($parent instanceof Document\Page && $parent->getAction() == 'post-list') {
                $folder = Document\Service::createFolderByPath($parent->getFullPath() . date('/Y/m/d'));
                $target->setParent($folder);
            }
        }
    }
}
