<?php

namespace Website\Tool;

use Pimcore\Model\Document;
use Pimcore\Model\Element\Tag;
use Pimcore\Db;

class Post
{

    public static function getCategories($tags = false)
    {
        $path = \Pimcore\Config::locateConfigFile('post-categories.php');

        try {
            $categories = include($path);
        }
        catch(\Exception $e) {
            throw new \Exception('config/post-categories.php is missing');
        }

        if ($tags) {
            // this isn't great, but was bolted on way after
            $tagArr = explode(',', $tags);
            $list = self::getByTagsAndCategories(PHP_INT_MAX, $tagArr);

            $categories = array_unique(array_map(function($v) {
                return $v->getElement('category')->getData();
            }, $list));
        }

        return $categories;
    }

    private static function getList($limit = 7, $paramConditionStr = '', $paramConditionArr = [], $exclude = [])
    {
        $items = new Document\Listing();

        // initialise condition data
        $conditionStr = '`type` = \'page\'';
        $conditionArr = [];

        // ensure just post articles are collected
        $conditionStr .= ' AND id IN (SELECT id FROM documents_page WHERE id = documents.id and action = \'post\')';

        if (count($exclude)) {
            foreach ($exclude as $excludeId) {
                $conditionStr .= ' AND documents.id != ?';
                $conditionArr[] = $excludeId;
            }
        }


        $conditionStr .= ' '.$paramConditionStr;
        $conditionArr = array_merge($conditionArr, $paramConditionArr);

        // apply all conditions
        $items->setCondition($conditionStr, $conditionArr);

        // order by date element
        $items->setOrderKey('(SELECT data FROM documents_elements WHERE documentId = documents.id AND name = \'date\') DESC', false);

        // apply limit
        if ($limit > 0) {
            $items->setLimit($limit);
        }

        return $items->load();

    }

    public static function getHomepage($limit = 5)
    {
        return self::getList($limit, ' AND id IN (SELECT documentId FROM documents_elements WHERE name = \'featured\' AND data = 1)');
    }

    public static function getChildren($limit = 5, $document, $search = false)
    {
        // restrict by document
        $condition = ' AND `path` LIKE ?';
        $pathGlob = $document->getRealFullPath() . '/%';
        $params = [$pathGlob];

        if ($search) {
            $titleFragments = [];
            $tagFragments = [];

            // restrict by title
            foreach (explode(' ', $search) as $term) {
                $titleFragments[] = 'data LIKE ?';
                $params[] = '%'.$term.'%';
            }
            // restrict by tags
            foreach (explode(' ', $search) as $term) {
                $tagFragments[] = '((SELECT id FROM tags WHERE name = ?) IN (SELECT tagid FROM tags_assignment WHERE cid = documents.id))';
                $params[] = $term;
            }

            $condition .= ' AND (id IN (SELECT documentId FROM documents_elements WHERE ' . implode(' OR ', $titleFragments) . ') OR ' . implode(' OR ', $tagFragments) . ')';
        }

        return self::getList($limit, $condition, $params);
    }

    private static function getTagConditions(array $tags)
    {
        $tagFilterStr = '
            (
                SELECT id FROM tags WHERE name = ?
            )
            IN
            (
                SELECT tagid FROM tags_assignment WHERE cid = documents.id
            )
        ';

        $conditionArr = [];
        $conditionStr = '';

        if (count($tags)) {
            $conditionStr = [];

            foreach ($tags as $tag) {
                $conditionArr[] = $tag;
                $conditionStr[] = $tagFilterStr;
            }

            $conditionStr = ' AND (' . implode(' OR ', $conditionStr) . ')';
        }
        else {
            // if no tags are passed, match nothing
            $conditionStr = ' AND false';
        }

        return (object) [
            'conditionStr' => $conditionStr,
            'conditionArr' => $conditionArr,
        ];
    }

    public static function getRelated($limit = 3, $document)
    {

        $tags = Tag::getTagsForElement('document', $document->getId());

        $tags = array_map(function($d) { return $d->getName(); }, $tags);

        $tagConditions = self::getTagConditions($tags);

        return self::getList($limit, $tagConditions->conditionStr, $tagConditions->conditionArr, [$document->getId()]);

    }

    public static function getByCategories($limit, $category = false)
    {
        $conditionStr = '';
        $conditionArr = [];

        if ($category) {
            $conditionStr .= 'AND id IN (SELECT documentId FROM documents_elements WHERE name = \'category\' AND data = ?)';
            $conditionArr[] = $category;
        }

        return self::getList($limit, $conditionStr, $conditionArr);
    }

}
