<?php /*

    params;

    page (Document) -> document to read article from

    example;

    <?= $this->template('/templates/post-card.php', [
        'page' => $object,
    ]) ?>

 */ ?>

<?php

    // get variables
    $post = $this->page;
    $author = $post->getElement('author') ? $post->getElement('author')->getElement() : null;
    $subtitle = (string) $post->getElement('subtitle');
    $subtitle = strlen($subtitle) > 120 ? substr($subtitle, 0, 120) . '...' : $subtitle;
    $hidePage = $post->getElement('hide-page') ? $post->getElement('hide-page')->isChecked() : null;
?>
    <div class="col-sm-12 col-md-4 mb-40">
        <div class="item">
            <a class="link" href="<?= $this->url(['document' => $post]); ?>"> 
                <?php if ($post->getElement('image') && $post->getElement('image')->getImage()) : ?>
                    <img class="image" src="<?= $post->getElement('image')->getThumbnail('blog-list') ?>"/>
                <?php else : ?>
                    <img class="image" src="/website/static/images/news/nfn_img.png">
                <?php endif; ?>
                <div class="content">
                    <?php if ($post->getElement('date') && $post->getElement('date')->getData()) : ?>
                        <span class="date">
                            <time datetime="<?= $post->getElement('date')->getData()->toIso8601String() ?>">
                                <?= $post->getElement('date')->getData()->format('F jS Y'); ?>
                            </time>
                        </span>
                    <?php endif; ?>
                    <h3 class="subheading bold">
                        <?= $subtitle ?>  
                    </h3>
                </div>
            read more <img class="arrow" src="/website/static/images/svgs/arrow_icon_red.svg"></a>
        </div>
    </div>

<?php unset($this->page); ?>
