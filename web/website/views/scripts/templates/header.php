<nav class="nav">
    <div class="container">
        <div class="row">
            <div class="col-8 col-md-3 align-self-center order-2 order-md-1">
                <div class="logo-wrapper mt-10">
                    <div class="logo-wrapper">
                        <img src="/website/static/images/svgs/gather_logo.svg" alt="Gather Digital"> 
                    </div>
                </div>
            </div>
            <div class="col-2 col-md-8 align-self-center order-1 order-md-0 d-block d-md-none">
                <div class="hamburger-wrap d-block d-md-none">
                    <svg class="hamburger" viewBox="0 0 20 16" width="20" height="16">
                        <g stroke="#E14851">
                            <line x1="0" x2="20" y1="1" y2="1" stroke-width="2"></line>
                            <line x1="0" x2="20" y1="8" y2="8" stroke-width="2"></line>
                            <line x1="0" x2="20" y1="15" y2="15" stroke-width="2"></line>
                        </g>
                    </svg>
                </div>
            </div>
            <div class="col-2 col-md-3 align-self-center order-3 order-md-3">
                <a class="btn btn-custom nav d-none d-md-block" href="#" data-toggle="modal" data-target="#contact">Contact
                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 10">
                        <g data-name="Layer 2">
                            <path d="M11.78 0a1.15 1.15 0 0 1 .86.36 1.15 1.15 0 0 1 .36.86v7.31a1.23 1.23 0 0 1-1.22 1.22H1.22a1.15 1.15 0 0 1-.86-.36A1.15 1.15 0 0 1 0 8.53V1.22A1.15 1.15 0 0 1 .36.36 1.15 1.15 0 0 1 1.22 0zm0 1.22H1.22v1Q2.11 3 4.65 5l.23.2a4.69 4.69 0 0 0 .81.61 1.53 1.53 0 0 0 .81.33 1.53 1.53 0 0 0 .81-.33 4.69 4.69 0 0 0 .81-.61l.23-.2q2.55-2 3.43-2.74zM1.22 8.53h10.56V3.81L8.84 6.14a7.65 7.65 0 0 1-1 .77 2.51 2.51 0 0 1-1.29.4 2.66 2.66 0 0 1-1.32-.4 11.43 11.43 0 0 1-1-.77L1.22 3.81z" fill="#fff" data-name="Layer 1"/>
                        </g>
                    </svg>                                
                </a>
                <div class="mail">
                    <a href="#" data-toggle="modal" data-target="#contact" class="d-block d-md-none"><img src="/website/static/images/svgs/envelope_icon_red.svg"></a>
                </div>
            </div>
            <div class="mobile-wrap col-12 col-md-6 align-self-center order-4 order-md-2">
                <div class="overlay-menu-content overlay-menu">
                    <ul class="nav-items">
                        <li class="d-none d-md-inline item"><a class="item-link" href="#">Case Studies</a></li>
                        <li class="d-none d-md-inline item"><a class="item-link" href="#">News</a></li>
                        <li class="d-block d-md-none item"><a class="item-link" href="#">Home</a></li>
                        <li data-custom-collapse class="d-block d-md-none item"><a class="item-link" href="#"> Services <span class="plus"></span></a></li>
                        <ul id="list" class="collapse-list">
                            <li class="collapse-item"><a href="#" class="link">Digital Product Development</a></li>
                            <li class="collapse-item"><a href="#" class="link">Website Desgin &amp; Development</a></li>
                            <li class="collapse-item"><a href="#" class="link">Mobile App Development</a></li>
                            <li class="collapse-item"><a href="#" class="link">Data Collection</a></li>
                            <li class="collapse-item"><a href="#" class="link">Product Information Management</a></li>
                            <li class="collapse-item"><a href="#" class="link">Analystics and Data Visualisation</a></li>
                            <li class="collapse-item"><a href="#" class="link">Augmented Reality</a></li>
                            <li class="collapse-item"><a href="#" class="link">Web-To-Print</a></li>
                        </ul>
                        <li data-custom-collapse class="d-block d-md-none item"><a class="item-link" href="#"> Work <span class="plus"></span></a></li>
                        <ul id="list" class="collapse-list">
                            <li class="collapse-item"><a href="#" class="link">Apopo</a></li>
                            <li class="collapse-item"><a href="#" class="link">No Filter Needed</a></li>
                            <li class="collapse-item"><a href="#" class="link">Rail Diary</a></li>
                            <li class="collapse-item"><a href="#" class="link">Lyco</a></li>
                            <li class="collapse-item"><a href="#" class="link">Koob</a></li>
                            <li class="collapse-item"><a href="#" class="link">Pinseeker</a></li>
                            <li class="collapse-item"><a href="#" class="link">SISU</a></li>
                            <li class="collapse-item"><a href="#" class="link">Manchester College</a></li>
                        </ul>
                        <li data-custom-collapse class="item"><a class="item-link" href="#"> Careers <span class="plus"></span></a></li>
                        <ul id="list" class="collapse-list">
                            <li class="collapse-item"><a href="#" class="link">Magento Developer</a></li>
                            <li class="collapse-item"><a href="#" class="link">Full Stack Developer</a></li>
                        </ul>
                        <li class="d-block d-md-none item"><a class="item-link" href="#">Contact</a></li>
                        <li class="item"><a class="item-link" href="#">About</a></li>
                        <li class="d-block d-md-none item"><a class="item-link" href="#">Terms &amp; Conditions</a></li>
                        <li class="d-block d-md-none item"><a class="item-link" href="#">License</a></li>
                    </ul>
                    <div class="content">
                        <ul class="social-media">
                            <li class="social-item"><a class="link" href="#"><img class="icon" src="/website/static/images/svgs/facebook_icon.svg" alt="Facebook Icon"></a></li>
                            <li class="social-item"><a class="link" href="#"><img class="icon" src="/website/static/images/svgs/twitter_icon.svg" alt="Twitter Icon"></a></li>
                            <li class="social-item"><a class="link" href="#"><img class="icon" src="/website/static/images/svgs/linkedin_icon.svg" alt="Linkedin Icon"></a></li>
                            <li class="social-item"><a class="link" href="#"><img class="icon" src="/website/static/images/svgs/instagram_icon.svg" alt="Instagram Icon"></a></li>
                        </ul>
                        <p class="text">&copy; Gather Digital Ltd</p>
                        <p class="text">A company registered in England and Wales
                            (Company No. 8322830)
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</nav>