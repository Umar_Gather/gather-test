<div class="block-large block-banner logo-list">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 offset-md-2">
                <span class="subtle text">
                    News
                </span>
                <h1 class="heading">
                    Keep up to date with our latest news, works and events here
                </h1>
            </div>
        </div>
    </div>
</div>

<section class="block-large block-news bg-grey">
    <div class="container">
        <div class="row">
            <?php for ($i = 0; $i < count($this->posts); $i++) : ?>
                <?= $this->template('/templates/post-card.php', [
                    'page' => $this->posts[$i],
                ]) ?>
            <?php endfor; ?>
        </div>
    </div>
</section>
