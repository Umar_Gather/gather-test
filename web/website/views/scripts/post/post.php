<?php
    $post = $this->document;
    $tags = \Pimcore\Model\Element\Tag::getTagsForElement('document', $post->getId());
    $author = $this->href('author')->getElement();
?>

<?php if ($this->editmode) : ?>
    <div class="editmode-topbar">
        <div class="row">
            <div class="col-sm-12">
                <span class="title">Author</span>
                <?= $this->href('author', ['classes' => ['PimcoreUser'], 'reload' => true]) ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="block-large block-banner logo-list">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 offset-md-2">
                <?php foreach ($tags as $tag) : ?>
                    <span class="subtle text">
                        <?= $tag->getName() . ',' ?>
                    </span>
                <?php endforeach; ?>
                <h1 class="heading">
                    <?= $this->input('subtitle'); ?>
                </h1>
                <span class="subheading red bold author">
                    <?= $author ? $author->getName() : 'Admin'; ?>
                </span>
            </div>
        </div>
    </div>
</div>

<?= $image = $this->image('image', ['class' => 'd-none']); ?>
<?php if (!$image->isEmpty()) : ?>
    <section class="block-large block-hero" style="background-image: url(<?= $image->getSrc(); ?>)"></section>
<?php endif; ?>

<section class="block-large block-article">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 offset-md-2">
                <article class="content 12">
                    <?php if ($this->editmode) : ?>
                        <p>Select the date of the blog post</p>
                        <?= $this->dateFixed("date"); ?>
                    <?php endif; ?>
                    <h2 class="subheading red bold">
                       <?=$this->input('heading'); ?>
                    </h3>
                    <?= $this->wysiwyg('content'); ?>

                    <ul class="tags">
                        <?php foreach ($tags as $tag) : ?>
                            <li class="tag"><?= $tag->getName() ?></li>
                        <?php endforeach; ?>
                    </ul>
                    <?= $this->textarea('additional-info', ['class' => 'text']); ?>

                    <?php if ($author) : ?>

                        <div class="author-wrapper">
                            <div class="row">
                                <div class="col-sm-2 col-md-2 align-self-center">
                                    <img class="author-head" src="<?= $author->getProfileImage(); ?>">
                                </div>
                                <div class="col-sm-5 col-md-5">
                                    <span class="author-header">Author</span>
                                    <span class="author-name"><?= $author->getName(); ?></span>
                                    <span class="author-position"><?= $author->getRole(); ?></span>
                                    <span class="social-leading">Follow on</span>
                                    <ul class="author-social">
                                        <?php if ($author->getFacebook() != '') : ?>
                                            <li class="item"><a href=""><img class="image" src="/website/static/images/svgs/facebook_icon.svg"></a></li>
                                        <?php endif; ?>
                                        <?php if ($author->getTwitter() != '') : ?>
                                            <li class="item"><a href=""><img class="image" src="/website/static/images/svgs/twitter_icon.svg"></a></li>
                                        <?php endif; ?>
                                        <?php if ($author->getLinkedin() != '') : ?>
                                            <li class="item"><a href=""><img class="image" src="/website/static/images/svgs/linkedin_icon.svg"></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="spacing">
                                        <p class="text">
                                            <?= $author->getAbout(); ?>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endif ;?>

                </article>
            </div>
        </div>
    </div>
</section>

