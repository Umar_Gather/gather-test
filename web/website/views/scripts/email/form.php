<?php
/**
 * @var \Pimcore\Model\Document\Email $document
 */
$document = $this->document;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html><head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"><title><?= $this->document->getSubject(); ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style type="text/css">
        #outlook a{padding:0;}
        .ReadMsgBody{width:100%;} .ExternalClass{width:100%;}
        .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}
        body, table, td, a{-webkit-text-size-adjust:100%; -ms-text-size-adjust:100%;}
        table, td{mso-table-lspace:0pt; mso-table-rspace:0pt;}
        img{-ms-interpolation-mode:bicubic;}

        body{margin:0; padding:0;}
        img{border:0; height:auto; line-height:100%; outline:none; text-decoration:none;}
        table{border-collapse:collapse !important;}
        body{margin:0; padding:0; min-width:100%; width:100% !important;}


        @media screen and (min-width: 651px) {
            table[class="responsive-table"]{width:600px !important; height:auto !important;}
        }

        @media screen and (max-width: 525px) {

            table[class="wrapper"]{
                width:100% !important;}

            td[class="logo"]{
                text-align: left;
                padding: 20px 0 20px 0 !important;}

            td[class="mobile-hide"]{
                display:none !important;}

            img[class="mobile-hide"]{
                display: none !important;}

            td[class="display-img"] {max-width: 100% !important; height: auto !important; display: block !important;}

            td[class="logo"] img{
                margin:0 auto!important;}

            img[class="img-max"]{
                max-width: 100%;
                height:auto;}

            table[class="responsive-table"]{
                width:100% !important;}

            table[class="responsive-table2"]{
                width:100%!important;
                border:0 !important;
            }

            table[class="responsive-button"]{
                width:100% !important;}

            table[class="responsive-webinar"]{
                width:50%!important;}

            td[class="padding"]{
                padding: 10px 5% 15px 5% !important;}

            td[]{
                padding: 0px 5% 10px 5% !important;
                text-align: left;}

            td[class="padding6"]{
                padding: 0px 5% 10px 5% !important;
                text-align: left;}

            td[class="section-padding"]{
                padding: 50px 15px 50px 15px !important;}

            td[]{
                padding: 50px 15px 0 15px !important;}

            td[class="section-padding5"]{
                padding: 0 8% 0 8% !important;}

            td[class="mobile-wrapper"]{
                padding: 10px 5% 15px 5% !important;}

            td[class="team-pad"]{
                padding: 10px 0 0 0 !important;}

            td[class="padding10"]{
                padding: 10px 0 !important;}

            table[class="responsive-button"] {
                width: 100% !important;}

            table[class="responsive-table2"]{
                width:100%!important;
                border:0 !important;
            }


        }
    </style>
    <!--[if gte mso 9]>
    <style type="text/css">
        #outlookholder {
            width:600px;
        }

    </style>
    <![endif]-->
</head>
<body>
    <table border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;" width="100%">
        <tbody bgcolor="#ffffff">
        <tr>
            <td bgcolor="#FFAD08">
                <div align="center" style="padding: 0px 15px 0px 15px;"><!--[if gte mso 9]>
                    <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center">
                        <tr>
                            <td>
                <![endif]--><!--[if (IE)]>
                    <table border="0" cellspacing="0" cellpadding="0" width="600" align="center">
                        <tr>
                            <td>
                <![endif]-->
                    <table border="0" cellpadding="0" cellspacing="0" class="wrapper" width="600">
                        <tbody>
                        <tr>
                            <td class="logo" style="padding: 20px 0px 20px 0px;">
                                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                    <tbody>
                                    <tr>
                                        <td align="left" bgcolor="#FFAD08" width="138"><a href="http://www.apopo.org/en/support"><img alt="" border="0" src="http://www.apopo.org/images/general/logo.png" style="display: block; color: #e28c05; font-size: 19px;" /></a></td>
                                        <td bgcolor="#FFAD08" class="mobile-hide">&nbsp;</td>
                                        <td align="right" bgcolor="#FFAD08" class="mobile-hide" width="300">
                                            <table border="0" cellpadding="0" cellspacing="0">
                                                <tbody>

                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <!--[if mso]>
                    </td></tr></table>
                <![endif]--><!--[if (IE)]>
                    </td></tr></table>
                <![endif]--></div>
            </td>
        </tr>
        </tbody>
    </table>

    <table  border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;" width="100%">
        <tbody>
        <tr>
            <td align="center" bgcolor="#FFAD08" style="padding: 10px 0 10px 0;"><!--[if gte mso 9]>
                <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td>
            <![endif]--><!--[if (IE)]>
                <table border="0" cellspacing="0" cellpadding="0" width="600" align="center">
                    <tr>
                        <td>
            <![endif]-->
                <table border="0" cellpadding="0" cellspacing="0" class="responsive-table" style="width: 100%; max-width: 600px !important;">
                    <tbody bgcolor="#ffffff">
                    <tr>
                        <td>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td>
                                        <table align="left" border="0" cellpadding="0" cellspacing="0" class="responsive-table2" width="600">
                                            <tbody>
                                                <tr>
                                                    <td align="center" style="font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#999999; -webkit-text-size-adjust: none" valign="middle"><br />
                                                        <img src="http://joinkoob.com/wp-content/uploads/2016/02/Koob-BBLT-sml.png">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="padding: 0 20px; font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#999999; -webkit-text-size-adjust: none" valign="middle"><br />
                                                        <b>name:</b> <?= $this->name; ?>
                                                    </td>
                                               </tr>
                                               <tr>
                                                <td align="left" style="padding: 0 20px; font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#999999; -webkit-text-size-adjust: none" valign="middle"><br />
                                                    <b>email:</b> <?= $this->email; ?>
                                                </td>
                                            </tr>
                                                <tr>
                                                    <td align="left" style="padding: 0 20px; font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#999999; -webkit-text-size-adjust: none" valign="middle"><br />
                                                        <b>phone:</b> <?= $this->phone; ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left" style="padding: 0 20px 10px 20px; font-size: 12px; line-height: 18px; font-family: Helvetica, Arial, sans-serif; color:#999999; -webkit-text-size-adjust: none" valign="middle"><br />
                                                        <b>message:</b> <?= $this->message; ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if mso]>
                </td></tr></table>
            <![endif]--><!--[if (IE)]>
                </td></tr></table>
            <![endif]--></td>
        </tr>
        </tbody>
    </table>

    <table border="0" cellpadding="0" cellspacing="0" style="table-layout: fixed;" width="100%">
        <tbody>
        <tr>
            <td align="center" bgcolor="#FFAD08" class="section-padding" style="padding: 0px 15px 35px 15px;"><!--[if gte mso 9]>
                <table id="outlookholder" border="0" cellspacing="0" cellpadding="0" align="center">
                    <tr>
                        <td>
            <![endif]--><!--[if (IE)]>
                <table border="0" cellspacing="0" cellpadding="0" width="600" align="center">
                    <tr>
                        <td>
            <![endif]-->
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                    <tbody>
                    <tr>
                        <td style="padding: 20px 0 0 0;">
                            <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tbody>
                                <tr>
                                    <td style="padding: 0;">
                                        <table align="center" border="0" cellpadding="0" cellspacing="0" class="responsive-table" width="600">
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <!--[if mso]>
                </td></tr></table>
            <![endif]--><!--[if (IE)]>
                </td></tr></table>
            <![endif]--></td>
        </tr>
        </tbody>
    </table>

</body>
</html>
