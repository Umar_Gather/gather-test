<div class="block-large block-banner bg-red">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-8 offset-md-2">
                <h1 class="heading white">
                    Magento Developer
                </h1>
            </div>
        </div>
    </div>
</div>

<section class="block-large block-text text-left">
    <div class="container">
        <div class="col-sm-12 col-md-8 offset-md-2">
            <h2 class="subheading red">
                Were currently looking for a skilled magento developer to join our studio in Manchester
            </h2>
            <p class="text">
                We’re currently looking for a skilled Magento developer to join our studio in Manchester. Working primarily on eCommerce projects, you will have a high level of understanding in the Magento platform both from a backend and frontend perspective. The ability to work with and integrate additional systems/technologies is important along with a good understanding of full stack in general.
            </p>
            <div class="subsection">
                <h3 class="subheading bold">
                    Required Experience
                </h3>
                <ol class="reqs">
                    <li class="req">3+ years commercial experience working with Magento</li>
                    <li class="req">Experience with Magento 2</li>
                    <li class="req">An ability to implement frontend template designs to a high standard</li>
                    <li class="req">A clear understanding of Git/Git-flow</li>
                </ol>
            </div> 
            <div class="subsection pb-0">
                <h3 class="subheading bold">
                    Attitude &amp; Perspecitve
                </h3>
                <ol class="reqs">
                    <li class="req">Looking to push the boundaries with new technology</li>
                    <li class="req">Looking to share knowledge / co-coding</li>
                </ol>
            </div>
            <div class="subsection pb-0">
                <p class="text">
                    Salary variable based on Experience
                </p>
            </div>
            <div class="subsection">
                <h3 class="subheading bold">
                    interested applications
                </h3>
                <a class="link bold" href="#" data-toggle="modal" data-target="#apply">Apply Here <img class="arrow" src="/website/static/images/svgs/arrow_icon_red.svg"></a>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="apply" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        <svg class="hamburger" data-dismiss="modal" viewBox="0 0 20 16" width="20" height="16">
                            <g stroke="#E14851">
                                <line x1="3" x2="17" y1="1" y2="15" stroke-width="2"></line>
                                <line x1="0" x2="20" y1="8" y2="8" stroke-width="2" style="opacity: 0;"></line>
                                <line x1="3" x2="17" y1="15" y2="1" stroke-width="2"></line>
                            </g>
                        </svg>
                    </span>
                </button>
                <div class="block-contact custom-modal">
                    <form class="form" id="apply-form">
                        <fieldset class="subsection">
                            <h2 class="subheading bold">
                                Welcome Developers
                            </h2>
                            <p class="text">
                                Complete this form if you are interested in joining us.
                            </p>
                        </fieldset>
                        <fieldset class="subsection">
                            <h3 class="subheading small">
                                Basic details
                            </h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="text-field" type="text" name="name" required/>
                                        <label class="control-label" for="name">Name</label><i class="bar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <input class="text-field" type="text" name="email" required/>
                                        <label class="control-label" for="email">Email</label><i class="bar"></i>
                                    </div>
                                </div>
                                <div class="col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <input class="text-field" type="text" name="phone" required/>
                                        <label class="control-label" for="phone">Phone</label><i class="bar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="text-field" type="text" name="portfolio" required/>
                                        <label class="control-label" for="portfolio">Portfolio link</label><i class="bar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input class="text-field" type="text" name="github" required />
                                        <label class="control-label" for="github">Github account link</label><i class="bar"></i>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="subsection">
                            <h3 class="subheading small">
                                CV upload
                            </h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="box">
                                        <div class="box__input">
                                            <input type="file" name="file" id="file" class="box__file" required/>
                                            <label for="file"><strong>Choose a file</strong><span class="box__dragndrop"> or drag it here</span>.</label>
                                        </div>  
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset class="subsection">
                            <h3 class="subheading small">
                                Questions
                            </h3>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea class="textarea-field" cols="10" name="question1" required></textarea>
                                        <label class="control-label" for="skills">Skills - tell us what you are great at</label><i class="bar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea class="textarea-field" cols="10" name="question2" required></textarea>
                                        <label class="control-label" for="dream">Dream job - where do you really want to work</label><i class="bar"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <textarea class="textarea-field" cols="10" name="question3" required></textarea>
                                        <label class="control-label" for="opinions">Opinion - Whats broken in the world</label><i class="bar"></i>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <div class="row">
                            <div class="col-sm-12">
                                <fieldset class="subsection">
                                    <button class="btn btn-custom bg-red alternate" type="submit">Submit
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 13 13.01">
                                                <defs>
                                                    <style>.cls-1{fill: #fff;}</style>
                                                </defs>
                                                <g id="Layer_2" data-name="Layer 2">
                                                    <g id="Layer_1-2" data-name="Layer 1">
                                                        <path class="cls-1" d="M12.8.09A.44.44,0,0,1,13,.55L11.14,11.69a.49.49,0,0,1-.23.33.46.46,0,0,1-.23.06.38.38,0,0,1-.17,0L6.68,10.48,4.52,12.85a.41.41,0,0,1-.34.16A.43.43,0,0,1,4,13a.44.44,0,0,1-.21-.17.43.43,0,0,1-.08-.27V9.26L.29,7.86A.45.45,0,0,1,.23,7l12.08-7A.42.42,0,0,1,12.8.09ZM10.32,11l1.6-9.6-10.4,6L4,8.36l6.27-4.64L6.75,9.5Z"/>
                                                    </g>
                                                </g>
                                            </svg>  
                                    </button>
                                </fieldset>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>