<?php

class Zend_View_Helper_Polyfill extends \Zend_View_Helper_Abstract
{
    // see https://polyfill.io/v2/docs/features/ for features

    private $features = [
        'default',
        'fetch',
        'Element.prototype.classList',
    ];

    /**
     * Returns a script link to a polyfill CDN
     * @return string
     */
    public function polyfill()
    {
        return '<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features='.join(',', $this->features).'"></script>';
    }
}
