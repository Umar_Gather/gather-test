<?php

class Zend_View_Helper_DateFixed extends \Zend_View_Helper_Abstract
{

    /**
     * So, for some bizarre reason, pimcore seems to accept a carbon style
     * formatting string in the pimcore admin panel, but a strftime one on the
     * user facing frontend. This is stupid wrapper for that.
     */
    public function dateFixed($name)
    {
        if ($this->view->editmode) {
            return $this->view->date($name, [
                 "format" => "F dS Y"
            ]);
        }
        else {
            return $this->view->date($name, [
                 "format" => "%B %e %Y"
            ]);
        }
    }
}
