<?php

use Website\Tool\Post;
use Website\Tool\SiteNav;

class PostController extends Website\Controller\Frontend
{
    public function init()
    {
        parent::init();

        $this->enableLayout();
    }

    public function siteSearchAction()
    {
        $this->view->hideBannerSnippet = true;
        $search = $this->getParam('search', false);
        $showAll = $this->getParam('all', false);

        if ($showAll || $search) {

            $results = Post::getChildren(-1, SiteNav::getRootDocument(), $search);

            $paginator = \Zend_Paginator::factory($results);
            $paginator->setCurrentPageNumber($this->getParam('page'));
            $paginator->setItemCountPerPage(8);
            $this->view->paginator = $paginator;

            $this->view->searchResults = iterator_to_array($paginator);

        }

    }

    public function postAction()
    {
        $this->view->related = Post::getRelated(3, $this->document);
        $this->view->hideBannerSnippet = true;
    }

    public function postListAction()
    {
        $search = $this->getParam('search', false);
        $showAll = $this->getParam('all', false);
        $this->view->search = $search;

        $hasNewsletterSignup = $this->view->checkbox('newsletter')->isChecked();
        $hasFeaturedPost = $this->view->checkbox('featured-post')->isChecked();
        $postQty = $hasNewsletterSignup ? 7 : 8;
        if (!$hasFeaturedPost) {
            $postQty++;
        }

        if ($showAll || $search) {

            $results = Post::getChildren(-1, $this->document, $search);

            $paginator = \Zend_Paginator::factory($results);
            $paginator->setCurrentPageNumber($this->getParam('page'));
            $paginator->setItemCountPerPage(8);
            $this->view->paginator = $paginator;

            $this->view->searchResults = iterator_to_array($paginator);

        } else {
            // no searching
            $this->view->posts = Post::getChildren($postQty, $this->document);

            // TODO: refactor
            $this->view->maxPostQuantity = count(Post::getChildren(-1, $this->document));
        }

    }

    public function postsAjaxAction()
    {
        $this->disableLayout();
        $this->removeViewRenderer();
        $tags = $this->getParam('tags', false);
        $category = $this->getParam('category', false);
        $limit = $this->getParam('limit', 5);
        $featured = $this->getParam('featured', false) == 'yes';

        $tags = array_map(function ($d) {
            return trim($d);
        }, explode(',', $tags));

        $posts = Post::getByTagsAndCategories($limit, $tags, $category);
        $markup = [];

        for ($i = 0; $i < count($posts); $i++) {
            $markup[] = $this->renderPostCard($posts[$i], !!($i == 0) && $featured);
        }

        $this->getResponse()
            ->setHeader('Content-type', 'text/html')
            ->appendBody(implode('', $markup));
    }

    private function renderPostCard($post, $isMain = false)
    {
        $view = new \Pimcore\View([
            'scriptPath' => './website/views/scripts/',
            'helperPath' => './website/views/helpers/'
        ]);

        $view->page = $post;
        $view->isMain = $isMain;

        return $view->render('/templates/post-card.php');
    }

}
