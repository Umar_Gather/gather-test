<?php

use Pimcore\Tool\Frontend;
use Pimcore\Model\Object;
use Pimcore\Model\Asset;
use Gather\Manager\EmailManager;

class FormController extends Website\Controller\Frontend
{

    public function getContactDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $allowedKeys = ['name', 'email', 'phone', 'message'];

            $newContact = new Object\ContactForm();
            $newContact->setParent(Object\Service::createFolderByPath('/contact/' . date('Y/m/d')));
            $newContact->setPublished(true);
            $newContact->setKey(\Pimcore\File::getValidFilename(time() . '-' . uniqid()));
            
            foreach($this->getAllParams() as $param=>$value) {
                if (!in_array($param, $allowedKeys)) {
                    continue;
                }
                $setter = 'set' . ucfirst($param);
                $newContact->$setter($value);
            }

            $newContact->save();

            EmailManager::getInstance()->triggerContactUsEmail($newContact);

            $this->toJSON([
                'success'  => true,
                'contacts' => [
                    (object) [
                        'name' => $newContact->getName() ?: '',
                        'email'    => $newContact->getEmail() ?: '',
                        'reason'   => $newContact->getPhone() ?: '',
                        'comment'  => $newContact->getMessage() ?: ''
                    ]
                ],
            ]);
        }
    }

    public function getVaccancyDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $allowedKeys = ['name','email','phone','portfolio', 'github' , 'question1', 'question2', 'question3'];

            $newApplicant = new Object\Vaccancy();
            $newApplicant->setParent(Object\Service::createFolderByPath('/applications/' . date('Y/m/d')));
            $newApplicant->setPublished(true);
            $newApplicant->setKey(\Pimcore\File::getValidFilename(time() . '-' . uniqid()));
            
            foreach($this->getAllParams() as $param=>$value) {
                if (!in_array($param, $allowedKeys)) {
                    continue;
                }
                $setter = 'set' . ucfirst($param);
                $newApplicant->$setter($value);
            }

            $file = $this->getParam('file');
            $cv = new Pimcore\Model\Asset();
            $cv->setFilename("myAsset.png");
            $data = file_get_contents($file);
            $cv->setData($data);
            $cv->setParent(Pimcore\Model\Asset::getByPath("/"));
            $cv->save();

            $newApplicant->save();

            EmailManager::getInstance()->triggerNewApplicantEmail($newApplicant);

            $this->toJSON([
                'success'  => true,
                'contacts' => [
                    (object) [
                        'name' => $newApplicant->getName() ?: '',
                        'email'    => $newApplicant->getEmail() ?: '',
                        'phone'   => $newApplicant->getPhone() ?: '',
                        'portfolio'  => $newApplicant->getPortfolio() ?: '',
                        'github'  => $newApplicant->getGithub() ?: '',
                        'question1'  => $newApplicant->getQuestion1() ?: '',
                        'question2'  => $newApplicant->getQuestion2() ?: '',
                        'question3'  => $newApplicant->getQuestion3() ?: ''
                    ]
                ],
            ]);
        }
    }

    private function toJSON($obj)
    {
        $this->_helper->json($obj, true);
    }
    
    private function sendEmail($submission)
    {

        $document = Document\Email::getByPath('/email/form');

        if (!$document) {
            throw new Exception('/email/form template is not set up');
        }

        $params = [
            'formName' => $submission->getFormName(),
            'formFields' => $submission->getFormFields(),
            'attachments' => array_map(function($d) {
                return $d->getFullPath();
            },$submission->getAttachments()),
        ];

        EmailManager::getInstance()
            ->sendEmail($document->getTo(), $document, $params);

    }
}
