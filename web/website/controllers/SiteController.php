<?php

class SiteController extends Website\Controller\Frontend
{
    public function init()
    {
        parent::init();

        $this->enableLayout();
    }

    public function defaultAction()
    {
    }

    public function kitchenSinkAction()
    {
    }

    public function vaccancyAction()
    {
    }
}
