<?php

chdir(__DIR__);
include_once("../pimcore/cli/startup.php");

ini_set('memory_limit', '1024M');
ini_set("max_execution_time", "-1");
define("PIMCORE_ADMIN", true);

//set the current user
/* \Zend_Registry::set("pimcore_admin_user", \Pimcore\Model\User::getByName('System.Import')); */

function out($message) {
    echo $message . PHP_EOL;
    Pimcore_Log_Simple::log(LOG_FILE_NAME, $message);
}

// This is useful for running quick scripts (batch jobs, etc).
// DO NOT USE IN PRODUCTION
