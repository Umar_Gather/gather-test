const webpack = require('webpack');
const ExtractText = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');

module.exports = (env={}, args={}) => {

    let outputFolder = env.static ||
        (!!env.dev ? '/bundles/' : '/../web/website/static/bundles');

    let config = {
        entry : {
            root: './components/root.js',
            styles: [
                './styles/root.scss',
            ],
        },
        output: {
            path:     __dirname + outputFolder,
            filename: '[name].js',
        },
        module: {
            rules: [
                {
                    test: /\.jsx?$/,
                    use: [
                        {
                            loader: 'babel-loader',
                            options: {
                                presets: babelPresets = [
                                    ['es2015', { modules: false }],
                                    'react', 'stage-0'
                                ],
                                plugins: [
                                    'transform-decorators-legacy',
                                    'transform-class-properties'
                                ]
                            }
                        }
                    ],
                },
                {
                    test: /\.scss/,
                    loader: ExtractText.extract({
                        fallback: 'style-loader',
                        use: [
                            { loader:'raw-loader' },
                            { loader:'sass-loader' },
                            { loader:'import-glob-loader' },
                            { loader: 'autoprefixer-loader?browsers=last 2 versions' },
                        ]
                    }),
                },
            ],
        },
        plugins: [
            new webpack.DefinePlugin({
                __DEV__: env.dev
            }),
            new webpack.ProvidePlugin({
                React: 'react',
            }),
            new StyleLintPlugin({
                configFile: '.stylelintrc',
                syntax: 'scss',
            }),
            new ExtractText('[name].css'),
        ],
        resolve: {
            extensions: ['.js', '.json', '.jsx'],
            alias: {
                '#util': __dirname + '/components/util',
            },
        },
        devtool: env.dev ? 'source-map' : false
    };

    if (env.dev) {
        // add linting
        config.module.rules.push({
            test: /\.js$/,
            enforce: 'pre',
            loader: 'eslint-loader',
            options: {
                configFile: '.eslintrc',
                failOnWarning: false,
                failOnError: false,
                emitError: false,
                fix: true
            }
        });
    }

    if (env.html) {
        // html live reload
        config.plugins.push(new HtmlWebpackPlugin({
            template: './templates/news-single.html',
            inject: false, // to allow them to be included manually
        }));
    }

    return config;
};
