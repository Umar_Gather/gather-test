// currently only works in X

export function swipe(element, callback) {

    let startX = void 0;

    function start(e) {
        let { screenX } = e.touches ? e.touches[0] : e;
        startX = screenX;
    }

    function end(e) {
        let { screenX } = e.changedTouches ? e.changedTouches[0] : e;
        let dx = screenX - startX;
        let distance = Math.abs(dx);
        let direction = !!(screenX > startX);
        callback({ distance, direction, dx });
    }

    element.addEventListener('touchstart', start);
    element.addEventListener('mousedown', start);
    element.addEventListener('touchend', end);
    element.addEventListener('mouseup', end);

}
