// animation primitives
// thom barlow

// Usage;
//
// tween((t) => {
//     const i = lerp(-20, 100, ease(t));
//     console.log(i);
// }, 500);

export function tween(callback, ticks = 500) {

    let timer = performance.now();

    ~function frame() {

        let diff = performance.now() - timer;

        if (diff <= ticks) {
            requestAnimationFrame(frame);
            callback(diff / ticks);
        }
        else {
            callback(1);
        }

    } ();

}

export function lerp(start, end, i) {
    return start + (end - start) * i;
}

export function ease(t) {
    // from https://gist.github.com/gre/1650294
    return t<.5 ? 2*t*t : -1+(4-2*t)*t;
}
