export function arrayUnique(arr) {
    return arr.filter((d, i, a) => a.indexOf(d) == i);
}
