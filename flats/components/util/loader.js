import { tween, ease } from '#util/animate';

const loader = Object
    .assign(
        document.createElement('div'),
        { className: 'loader-container' },
    )
    .appendChild(
        Object.assign(
            document.createElement('img'),
            { src: '/website/static/images/loader.gif' },
        )
    )
    .parentNode;

function getElement() {
    return document.querySelector('.loader-container') || (
        document.body.appendChild(loader)
    );
}

export function showLoader() {
    const DOMObject = getElement();
    tween((t) => {
        DOMObject.style.opacity = t / 1.5;
    });
}

export function hideLoader() {
    const DOMObject = getElement();
    tween((t) => {
        DOMObject.style.opacity = (1-t) / 1.5;

        if (t >= 1) {
            DOMObject.parentNode.removeChild(DOMObject);
        }
    });
}
