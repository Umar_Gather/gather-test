export function transformReact(str) {
    return {
        WebkitTransform: str,
        MozTransform: str,
        msTransform: str,
        OTransform: str,
        transform: str
    };
}

export function transformCSS(node, str) {
    Object.assign(node.style, {
        webkitTransform: str,
        mozTransform: str,
        msTransform: str,
        oTransform: str,
        transform: str
    });
}