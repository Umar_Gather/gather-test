export function matchHeights(nodeList) {
    const nodes = Array.from(nodeList);

    const maxHeight = nodes.reduce((a, b) => {
        b.style.height = 'initial';
        return Math.max(a, b.scrollHeight);
    }, 0);
    nodes.forEach((node) => {
        node.style.height = maxHeight + 'px';
    });

    return maxHeight;
}
