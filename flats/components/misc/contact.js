const contactFormWrapper = document.querySelector('.modal#contact');
if (contactFormWrapper) {
    const contactForm = contactFormWrapper.querySelector('#contact-form');
    contactForm.addEventListener('submit', (e) => {
        e.preventDefault();

        const formData = new FormData(contactForm);

        fetch('/contact-form/', {
            credentials: 'include',
            method: 'POST',
            body: formData,
        })
        .then((res) => res.text())
        .then((res) => {
            if (res.success) {
                console.log(res.success);
            }
            else if (res.error) {
                console.log(res.error);
            }
        });
    });
}