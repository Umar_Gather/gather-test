const applicationFormWrapper = document.querySelector('.modal#apply');
if (applicationFormWrapper) {
    const applicationForm = applicationFormWrapper.querySelector('#apply-form');
    applicationForm.addEventListener('submit', (e) => {
        e.preventDefault();

        const formData = new FormData(applicationForm);

        fetch('/application-form/', {
            credentials: 'include',
            method: 'POST',
            body: formData,
        })
        .then((res) => res.json())
        .then((res) => {
            if (res.success) {
                console.log(res.success);
            }
            else if (res.error) {
                console.log(res.error);
            }
        });
    });
}