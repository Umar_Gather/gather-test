import { transformCSS } from '#util/transform';
import { tween, lerp, ease } from '#util/animate';

const burger = document.querySelector('.hamburger-wrap .hamburger');
const burgerLines = burger && [...burger.querySelectorAll('line')];
const mobileWrapper = document.querySelector('.mobile-wrap');
const mobileNav = mobileWrapper && mobileWrapper.querySelector('.overlay-menu-content');

let menuOpen = false;

function toggleMenu() {
    menuOpen = !menuOpen;
    burger.classList.toggle('open');
    mobileWrapper.style.display = 'block';

    tween((t) => {
        // hamburger
        const i = ease(menuOpen ? t : 1 - t);
        burgerFlipper(i);

        // menu
        const transform = ease(!menuOpen ? t : 1 - t) * 100;
        transformCSS(mobileNav, `translateX(${-transform}%)`);

        // on end
        if (t >= 1) {
            if (!menuOpen) {
                mobileWrapper.style.display = 'none';
            }
        }
    }, 250);
}

function burgerFlipper(i) {
    const [a, b, c] = burgerLines;
    b.style.opacity = 1 - i;
    a.setAttribute('y2', 1+(i*14));
    a.setAttribute('x1', i*3);
    a.setAttribute('x2', 20-(i*3));
    c.setAttribute('y2', 15-(i*14));
    c.setAttribute('x1', i*3);
    c.setAttribute('x2', 20-(i*3));
}

if (burger) {
    burger.addEventListener('click', toggleMenu);

    window.addEventListener('resize', (e) => {
        if (window.innerWidth > 767) {
            transformCSS(mobileNav, null);
            mobileWrapper.style.display = null;

            if (menuOpen) {
                menuOpen = false;
                burgerFlipper(0);
            }
        }
        else {
            if (!menuOpen) {
                transformCSS(mobileNav, 'translateX(-100%)');
            }
        }
    });
}

if (window.innerWidth < 767) {
    mobileNav.style.transform = 'translateX(-100%)';
}