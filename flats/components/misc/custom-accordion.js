import { preload } from '#util/preload';
import { tween, lerp, ease } from '#util/animate';

let lists = [];

[...document.querySelectorAll('[data-custom-collapse]')]
    .forEach((node) => {
        let obj = {
            node,
            open: true,
            content: node.nextElementSibling,
            span: node.firstElementChild.firstElementChild,
        };
        node.addEventListener('click', (e) => {
            toggle(obj);
        });

        lists.push(obj);
    });



function toggle(obj, instant = false) {
    if (window.innerWidth > 576) return;
    const { node, open, content, span } = obj;
    if (instant && open) {
        content.style.height = 0;
    } else if (instant) {
        content.style.height = null;
    } else if (open) {
        const { height } = content.getBoundingClientRect();
        tween((t) => {
            const i = lerp(height, 0, ease(t));
            content.style.height = i + 'px';
        }, 500);
        span.classList.remove('active');

    } else {
        content.style.height = null;
        const { height } = content.getBoundingClientRect();
        content.style.height = 0;
        tween((t) => {
            const i = lerp(0, height, ease(t));
            content.style.height = i + 'px';
            if (t >= 1) {
                content.style.height = null;
            }
        });
        span.classList.add('active');
    }
    obj.open = !obj.open;
}

function resetHeight(obj){
    const { content, open } = obj;
    if (window.innerWidth > 576) {
        content.style.height = 0;
        obj.open = false;
    }
}

lists.forEach((obj) => {
    toggle(obj, true);
    document.addEventListener('resize', resetHeight(obj));
});
