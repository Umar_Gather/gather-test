const nav = document.querySelector('nav');
const navAccordions = nav.querySelectorAll('[data-toggle="collapse"]');

navAccordions.forEach((acc) => {
    acc.addEventListener('click', (e) => {
        acc.firstElementChild.classList.toggle('active');
        e.preventDefault();
        e.stopPropagation();
    });
});