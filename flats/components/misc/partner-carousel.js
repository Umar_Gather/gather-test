import { preload } from '#util/preload';
import { swipe } from '#util/swipe';
import { transformCSS } from '#util/transform';
import { tween, ease, lerp } from '#util/animate';
import { matchHeights } from '#util/match-heights';
import { throttle } from '#util/throttle';

const carousels = Array.from(
    document.querySelectorAll('.data-carousel-promotions'),
);

const isDebug = /debug/.test(location.search || '') || typeof pimcore !== 'undefined';

carousels.forEach((wrapper) => {
    const sliderWrappers = Array.from(wrapper.querySelectorAll('.promotion-item'));
    const container = wrapper.querySelector('.carousel-container-100');
    const interval = 3000;

    let timer = void 0;
    let isMobile = void 0;
    let startOffset = void 0;
    let incOffset = void 0;
    let slideIndex = (window.innerWidth < 768) ? 0 : 2;
    let slideQty = sliderWrappers.length;

    function startTimer() {
        timer = setInterval(() => {
            (!isDebug) && animateSlide();
        }, interval);
    }

    function stopTimer() {
        clearInterval(timer);
        timer = void 0;
    }

    function animateSlide(reverse = false, user = false) {
        const noDesktop = (slideQty < 6) && !isMobile;
        const noMobile = (slideQty < 3) && isMobile;
        const isUserAndExtreme = user && ((reverse && !slideIndex) || (!reverse && slideIndex == slideQty-1));
        if (noMobile || noDesktop || isUserAndExtreme) {
            return;
        }

        let lastSlideIndex = slideIndex;
        if (reverse) {
            slideIndex = !slideIndex ? slideQty-1 : slideIndex-1;
        }
        else {
            slideIndex = (slideIndex+1) % (slideQty);
        }

        tween((t) => {
            const i = lerp(lastSlideIndex, slideIndex, ease(t)) * incOffset;
            transformCSS(container, `translateX(${startOffset + (-i)}%)`);
        }, interval / 10);
    }

    function initPos() {
        sliderWrappers.forEach((wrap) => {wrap.style.width = incOffset + '%';});
        transformCSS(container, `translateX(${startOffset + (incOffset * -slideIndex)}%)`);
    }

    swipe(wrapper, throttle(({ direction, distance }) => {
        if (distance > 50) {
            stopTimer();
            animateSlide(direction, true);
            startTimer();
        }
    }, interval / 10));

    const responder = () => {
        stopTimer();

        if (window.innerWidth < 768 && (!isMobile || isMobile === void 0)) {
            isMobile = true;
            incOffset = 50;
            startOffset = 25;
            if (slideQty < 3) {
                startOffset = ((2-slideQty) * incOffset)/2;
                slideIndex = 0;
            }
            initPos();
        }
        else if (window.innerWidth >= 768 && (isMobile || isMobile === void 0)) {
            isMobile = false;
            incOffset = 20;
            startOffset = 40;
            // handle edge cases
            if (slideQty < 6) {
                startOffset = ((5-slideQty) * incOffset)/2; // to centre the slides
                slideIndex = 0;
            }

            initPos();
        }
        
        startTimer();
    };

    window.addEventListener('resize', responder);
    window.addEventListener('load', responder);
    window.addEventListener('DOMContentLoaded', responder);

});
