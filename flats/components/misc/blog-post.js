const blogPost = document.querySelector('.post.single');
let isInViewport = function ( elem ) {
    let distance = elem.getBoundingClientRect();
    return (
        distance.top >= 0 &&
        distance.left >= 0 &&
        distance.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
        distance.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
};

if (blogPost) {
    document.addEventListener('scroll', () => {
        if (isInViewport(blogPost)){
            blogPost.classList.add('in-view');
        } else {
            blogPost.classList.remove('in-view');
        }
    });
}

