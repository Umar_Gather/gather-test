(new WebSocket(`ws://${location.hostname}:3000/`))
    .addEventListener('message', (e) => {
        if (e.data == 'RELOAD') {
            location.reload();
        }
    });

console.info('Development mode; waiting for updates...');

// nuke pimcore's onbeforeunload admin panel function

if (typeof pimcore == 'object' && Ext) {
    Ext.onReady(() => {
        ~function loop() {
            setTimeout(() => {
                if (editablesReady) {
                    window.onbeforeunload = function(e) {};
                }
                else {
                    loop();
                }
            }, 50);
        } ();
    });
}
