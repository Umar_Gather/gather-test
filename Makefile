build: build_web

clean:
	rm -rf ./release/*
	rm -rf ./vendor/*

install: install_composer_dependencies

build_web: install_optimised
	echo "Removing Unneeded Temp Files ..."
	rm -rf ./vendor/byng/pimcore-composer-installer/cache/*
	echo "Building web artifact..."
	tar czf ./release/pimcore-web.tar.gz -C ./ \
	./vendor \
	./web
	echo "Built web artifact"

install_composer_dependencies:
	composer install
	cd ./flats && npm install && npm run deploy

install_optimised:
	composer install && composer dump-autoload -o
	cd ./flats && npm install && npm run deploy

.PHONY: build clean install build_web install_composer_dependencies install_optimised
.SILENT:
