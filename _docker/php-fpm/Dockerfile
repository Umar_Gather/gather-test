FROM phpdockerio/php7-fpm:latest

# Install selected extensions and other stuff
RUN apt-get update \
    && apt-get -y --no-install-recommends install \
    git \
    curl \
    bzip2 \
    unzip \
    build-essential \
    autoconf \
    cabextract \
    php7.0-common \
    php7.0-curl \
    php7.0-mysql \
    php7.0-redis \
    php7.0-sqlite3 \
    php7.0-bcmath \
    php7.0-bz2 \
    php7.0-dbg \
    php7.0-gd \
    php7.0-json \
    php7.0-imagick \
    php7.0-imap \
    php7.0-intl \
    php7.0-ldap \
    php7.0-mbstring \
    php7.0-opcache \
    php7.0-soap \
    php7.0-ssh2 \
    php7.0-xmlrpc \
    php7.0-xml \
    php7.0-zip \
    && apt-get clean; rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /usr/share/doc/*

# install composer
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php --install-dir=/usr/bin --filename=composer \
    && php -r "unlink('composer-setup.php');"

# install node & npm
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash - && apt-get install -y nodejs

# add developer tools
ADD _docker/php-fpm/clean-pimcore.sh /usr/local/bin/clean-pimcore
RUN chmod +x /usr/local/bin/clean-pimcore

WORKDIR "/application"
