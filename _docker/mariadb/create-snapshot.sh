#!/usr/bin/env bash

OUT_PATH="/application/_docker/database-snapshots"
OUT_TMPFILE="pimcore_database_nonportable.sql"
OUT_FILENAME="snapshot-$(date +%F_%R).sql"

echo "Dumping DB to $OUT_PATH/$OUT_FILENAME ..."
mysqldump -u root -p$MYSQL_ROOT_PASSWORD $MYSQL_DATABASE > "$OUT_PATH/$OUT_TMPFILE"
echo "Making DB Portable..."
sed -E 's/DEFINER=`[^`]+`@`[^`]+`/DEFINER=CURRENT_USER/g' "$OUT_PATH/$OUT_TMPFILE" > "$OUT_PATH/$OUT_FILENAME"
rm -f "$OUT_PATH/$OUT_TMPFILE"

echo "Snapshot created in $OUT_PATH/$OUT_FILENAME"
