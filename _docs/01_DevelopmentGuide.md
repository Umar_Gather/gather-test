## Development Setup

This project is powered by Docker! To get started follow these steps:

#### 1) Install Docker CE
Download the latest **Stable** version of Docker CE for your Operating System. Follow each distribution's instructions to install.
 * Windows: https://store.docker.com/editions/community/docker-ce-desktop-windows
 * Mac: https://store.docker.com/editions/community/docker-ce-desktop-mac
 * Ubuntu: https://store.docker.com/editions/community/docker-ce-server-ubuntu
 * Debian: https://store.docker.com/editions/community/docker-ce-server-debian
 * Anything else, http://gph.is/2sNwWS9

#### 2) Copy the Database to you working directory
Copy the most up to date version of the project database to the `_docker/appdata directory`.
This file can be .sql .sh .tar.gz e.g. `_docker/appdata/pimcore_portable.sql`

#### 3) Build the container
Once Docker is installed, `cd` to the project root folder and run `docker-compose up -d`.
This will initialise and start all the containers, then leave them running in the background.

##### Verify all containers are running

    docker container list

#### 4) Configure Pimcore Environment

Fire up a shell in the php container and run the project Makefile

    docker-compose exec php-fpm bash
    make install
    exit

#### 5) Run a few pimcore deployment tasks to finalise the setup

Now we need to refresh a few files in order for Pimcore to function properly, so fire up
another shell in the php-fpm container again (This may take a minute or two to become available):

    docker-compose exec php-fpm clean-pimcore

### You're Done!
Visit `http://localhost:1337` to see our running pimcore application

To develop in the application, just use your working copy, no upload required.

#### More info

Service|Hostname|Port number
------|---------|-----------
php-fpm|php-fpm|9000
MariaDB|mariadb|3306 (default)
Redis|redis|6379 (default)

#Docker compose cheatsheet#

**Note:** you need to cd first to where your docker-compose.yml file lives.

  * Start containers in the background: `docker-compose up -d`
  * Start containers on the foreground: `docker-compose up`. You will see a stream of logs for every container running.
  * Stop containers: `docker-compose stop`
  * Kill containers: `docker-compose kill`
  * View container logs: `docker-compose logs`
  * Execute command inside of container: `docker-compose exec SERVICE_NAME COMMAND` where `COMMAND` is whatever you want to run. Examples:
        * Shell into the PHP container, `docker-compose exec php-fpm bash`
        * Run symfony console, `docker-compose exec php-fpm bin/console`
        * Open a mysql shell, `docker-compose exec mysql mysql -uroot -pCHOSEN_ROOT_PASSWORD`

#Recommendations#

It's hard to avoid file permission issues when fiddling about with containers due to the fact that, from your OS point of view, any files created within the container are owned by the process that runs the docker engine (this is usually root). Different OS will also have different problems, for instance you can run stuff in containers using `docker exec -it -u $(id -u):$(id -g) CONTAINER_NAME COMMAND` to force your current user ID into the process, but this will only work if your host OS is Linux, not mac. Follow a couple of simple rules and save yourself a world of hurt.

  * Run composer outside of the php container, as doing so would install all your dependencies owned by `root` within your vendor folder.
  * Run commands (ie Symfony's console, or Laravel's artisan) straight inside of your container. You can easily open a shell as described above and do your thing from there.

